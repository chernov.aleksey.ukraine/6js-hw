/*Опишите своими словами как работает цикл forEach.
Используется для перебора массива на подобии for - of, ни че не возвращает, но дает возможность 
выполнить одну и ту же функцию (обычно пользуют функции типа callback) с каждым элементом массива*/



let id = Symbol();
let initialArr = ['hello', { 1: "some", 3: 3, 5: 'thing' }, true, undefined, 23, [1, 2, 3], null, 123n, id,];
let typeOfData = prompt('Enter the type of the data: string, number, object, array, null, undefined, bigint, boolean or symbol');

console.log(filterOfArray(initialArr, typeOfData));

function filterOfArray(arr, type) {
console.log(arr);
console.log('IS FILTERED WITH ' + type);
    const nullValues = [null,];
    switch (type) {
        case 'null':
            return arr.filter((element) => !nullValues.includes(element));
        case 'array':
            return arr.filter((element) => !Array.isArray(element));
        case 'object':
            return arr.filter((element) => ( (typeof element !== 'object') || Array.isArray(element) || nullValues.includes(element) ) );
        default:
            return arr.filter((element) => (typeof element !== type ));
    }
}

